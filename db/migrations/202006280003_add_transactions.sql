create table expense.transactions(
	id serial primary key,
	transaction_date date not null,
	transaction_description text not null,
	transaction_amount bigint not null,
	posting_date date not null,
	currency_code varchar(10) not null,
	foreign_currency_amount bigint not null,
	merchant_category_code varchar(4) REFERENCES expense.merchant_codes(merchant_code),
	retail_transaction_code varchar(10) not null
);