import psycopg2
import requests
from http import HTTPStatus
import os


def get_card_id():
    # TODO fill in the request to get credit card ID
    return '72ca9605-d6df-43e6-9c4e-e16321b81eb9'


def get_access_token():
    # TODO fill the actual get access token
    return 'ccf0d9b05078c7042d6022120a057554'


def fetch_card_details():
    headers = {'Authorization': f'Bearer {get_access_token()}'}
    params = {'cardId': get_card_id()}
    r = requests.get(
        'https://api.ocbc.com:8243/transactional/creditcardhistory/1.0',
        params=params,
        headers=headers
    )
    if r.status_code != HTTPStatus.OK:
        raise BaseException(
            "Fail to retrived bank transactions",
            r.status_code,
            r.text
        )
    return r.json()['results']['creditCardTransactions']


def save_cards_transactions(cards):
    DB_NAME = os.getenv('DB_NAME')
    DB_USER = os.getenv('DB_USER')
    DB_PASSWORD = os.getenv('DB_PASSWORD')
    DB_HOST = os.getenv('DB_HOST')
    DB_PORT = os.getenv('DB_PORT')

    con = psycopg2.connect(
        database=DB_NAME, user=DB_USER,
        password=DB_PASSWORD, host=DB_HOST, port=DB_PORT
    )
    print("Connection to DB open successfully")
    cur = con.cursor()

    for card in cards:
        for txn in card['creditCardTransactionDetail']:
            cur.execute(
                """
                INSERT INTO expense.transactions
                (
                    transaction_date,
                    transaction_description,
                    transaction_amount,
                    posting_date,
                    currency_code,
                    foreign_currency_amount,
                    merchant_category_code,
                    retail_transaction_code
                ) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)
                """,
                (
                    txn['transactionDate'],
                    txn['transactionDescription'],
                    txn['transactionAmount'],
                    txn['postingDate'],
                    txn['currencyCode'],
                    txn['foreignCurrencyAmount'],
                    txn['merchantCategoryCode'],
                    txn['retailTransactionCode'],
                )
            )
    con.commit()
    print("Record inserted successfully")
    con.close()


def configure():
    current_env = os.getenv("ENV")
    if current_env == None or current_env == 'development':
        os.environ['DB_NAME'] = 'analytic'
        os.environ['DB_USER'] = 'postgres'
        os.environ['DB_PASSWORD'] = ''
        os.environ['DB_HOST'] = 'localhost'
        os.environ['DB_PORT'] = '5432'


def main():
    configure()
    cards = fetch_card_details()
    save_cards_transactions(cards)


main()
